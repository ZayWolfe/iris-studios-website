(function (window, document) {
    var data = {};
    
    var Icon = function (name, icon, path){
        this.name = name;
        this.icon = icon
        
        var obj = document.createElement("div");
        obj.className = "icon";
        obj.style.backgroundImage = "url(" + icon + ")";
        obj.onclick = function(){ getPage(path) };
        
        $("nav").appendChild(obj);
    }
    
    function getPage (path) {
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", path ,false);
        xmlhttp.send();
        $("content").update(xmlhttp.responseText);
        
    }
    
    function start (){
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET","data.json",false);
        xmlhttp.send();
        data = JSON.parse(xmlhttp.responseText);
        
        for(var key in data){

            if(data[key].real){
                new Icon(data[key].name, data[key].icon, data[key].path);
                
                if(key == "Home")
                    getPage(data[key].path);
            }
        }
        
    }
    
    window.onload = start;
})(window, document);